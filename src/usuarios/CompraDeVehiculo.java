/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package usuarios;

import java.time.LocalDate;
import java.util.Objects;
import sistema.Vehiculo;

/**
 *
 * @author VICTOR RODRIGUEZ
 */
public class CompraDeVehiculo {
    
    private Vehiculo vehiculo;
    private int cantidad;
    private LocalDate fechaCompra;
    private Cliente cliente;
    private int ID;
    public static int counter=0;
    //private MedioDePago medioDePago;

    // constructor de la clase para acceder y crear objetos
    public CompraDeVehiculo(Vehiculo vehiculo, int cantidad) {
        this.vehiculo = vehiculo;
        this.fechaCompra= null;
        this.cliente=null;
        this.cantidad = cantidad;
        //this.metodoPago =metodoPago;
        this.ID = counter+1;
        counter++;
    }

    public Vehiculo getVehiculo() {
        return vehiculo;
    }

    public void setVehiculo(Vehiculo vehiculo) {
        this.vehiculo = vehiculo;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public LocalDate getFechaCompra() {
        return fechaCompra;
    }

    public void setFechaCompra(LocalDate fechaCompra) {
        this.fechaCompra = fechaCompra;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public static int getCounter() {
        return counter;
    }

    public static void setCounter(int counter) {
        CompraDeVehiculo.counter = counter;
    }
    
    
    public double getPrecioCompraDeVehiculo(){
        return vehiculo.getPrecio()*cantidad;
    }

    @Override
    public String toString() {
        return "CompraDeVehiculo{" + "vehiculo=" + vehiculo + ", cantidad=" + cantidad + ", fechaCompra=" + fechaCompra + ", cliente=" + cliente + ", ID=" + ID + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CompraDeVehiculo other = (CompraDeVehiculo) obj;
        if (this.cantidad != other.cantidad) {
            return false;
        }
        if (this.ID != other.ID) {
            return false;
        }
        if (!Objects.equals(this.vehiculo, other.vehiculo)) {
            return false;
        }
        if (!Objects.equals(this.fechaCompra, other.fechaCompra)) {
            return false;
        }
        if (!Objects.equals(this.cliente, other.cliente)) {
            return false;
        }
        return true;
    }
    
    
}
