/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package usuarios;

import concesionaria.Color;
import java.util.ArrayList;
import java.util.Objects;
import sistema.Vehiculo;

/**
 *
 * @author VICTOR RODRIGUEZ
 */
public class Whislist {

     private ArrayList<CompraDeVehiculo> posiblesCompras;
    
    public Whislist() {
        posiblesCompras = new ArrayList<>();
        //this.posiblesCompras= posiblesCompras;
    }
    
    public double getPrecioTotal(){
        int total=0;
        for(int j=0;j<posiblesCompras.size();j++){
            total+= posiblesCompras.get(j).getPrecioCompraDeVehiculo();
        }
        return total;
    }
    
    public void anadirAlCarrito(CompraDeVehiculo c){
        if(existeEnCarrito(c)==true){
            aumentarCantidad(c);
        }
        else{posiblesCompras.add(c);}
    }
    
    public boolean existeEnCarrito(CompraDeVehiculo cc){
        for(int j=0;j<posiblesCompras.size();j++){
        if(cc.getVehiculo().equals(posiblesCompras.get(j))){
            return true;
        }
        }
        return false;
    }
    
    /*public void eliminarPorCodigo(String s){
        for(int j=0;j<posiblesCompras.size();j++){
            if(posiblesCompras.get(j).mismoCodigo(s)){
                posiblesCompras.remove(posiblesCompras.get(j));
                System.out.println("Se elimino el elemento del carrito.");
            }
            else{System.out.println("No existe elemento con el codigo ingresado.");}
        }
            
    }*/
    
    public void aumentarCantidad(CompraDeVehiculo cc){
        for(int j=0;j<posiblesCompras.size();j++){
        if(cc.getVehiculo().equals(posiblesCompras.get(j))){
            int k= posiblesCompras.get(j).getCantidad();
            posiblesCompras.get(j).setCantidad(cc.getCantidad()+k);
        }
    }}
    
     public void mostrarCarrito(){
        double total=0;
        System.out.printf("%-15s %-15s %-15s %-15s %-15s %-15s",Color.ANSI_RED+"Cod",Color.ANSI_RED+" Nombre",Color.ANSI_RED+"Cantidad",Color.ANSI_RED+"P.Unitario",Color.ANSI_RED+"Subtotal");
        for(CompraDeVehiculo c: posiblesCompras){
            Vehiculo p= c.getVehiculo();
            total+=c.getPrecioCompraDeVehiculo();
            System.out.printf("%-15s %-15s %-15s %-15s %-15s %-15s",p.getMarca(),p.getModelo(),String.valueOf(c.getCantidad()),String.valueOf(p.getPrecio()));
        }
        System.out.println("Total carrito:"+ String.valueOf(total));
    }
        
    public ArrayList<CompraDeVehiculo> getPosiblesCompras() {
        return posiblesCompras;
    }

    public void setPosiblesCompras(ArrayList<CompraDeVehiculo> posiblesCompras) {
        this.posiblesCompras = posiblesCompras;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Whislist other = (Whislist) obj;
        if (!Objects.equals(this.posiblesCompras, other.posiblesCompras)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Carrito{" + "posiblesCompras=" + posiblesCompras + '}';
    }
    
    public void quitarDelCarrito(CompraDeVehiculo c){
        posiblesCompras.remove(c);
    }
    
}
