/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package usuarios;

import acceso.Login;
import acceso.ManejoArchivos;
import static acceso.ManejoArchivos.lecturaArchivoSimple;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import sistema.Vehiculo;

/**
 *
 * @author eveyusol
 */
public class Supervisor extends User{
    Scanner sc = new Scanner (System.in);
    public static final String ruta = "src\\Archivos\\Usuarios.txt";
    
    public Supervisor(String name, String lastname, String password, String Cuenta) {
        super(name, lastname, password, Cuenta);
    }

    @Override
    public void revisarSolicitud() {
        if(Cliente.comprasRealizadas.size()>0){
            System.out.println("Existen peticiones de compras que tiene que revisar");
            int i = 1;            
            for(Vehiculo v: Cliente.comprasRealizadas){                
                System.out.println(i+" Marca: "+v.getMarca()+ " Modelo: "+v.getModelo()+" Año de fabricación: " +v.getAñoFabricacion()+" Tipo de combustible: "+v.getTipoCombustible()+" Estado: "+v.getEstado()+" Precio: "+v.getPrecio());
                System.out.println("El valor a pagar del cliente es: " + v.getPrecio());                
                i++;
            }
            i=1;
            System.out.println("Elija el numero del vehiculo que desea aceptar la petición de compra:");            
            String var= "";
            while(var.equals("") || Integer.parseInt(var)<0){
                Scanner sc = new Scanner(System.in);
                var = sc.nextLine();
                var=var.trim();
            }
            System.out.println("¿Desea aceptar el pedido del cliente? ");
            String var1= "";
            //Validando que el ingreso sea lo correcto
            while(!(var1.equals("si") || var1.equals("no"))){
                Scanner sc = new Scanner(System.in);
                var1 = sc.nextLine();
                //System.out.println(var1);
                //System.out.println(!(var1.equals("si") || var1.equals("no")));              
            }
            if(var1.equals("si")){
                System.out.println("Será informado el cliente de su compra");
                try{
                    Vehiculo v = Cliente.comprasRealizadas.get(Integer.parseInt(var)-1);
                    CompraDeVehiculo cv = new CompraDeVehiculo(v,1);
                    Cliente.comprasAceptadas.add(cv);
                    Cliente.comprasRealizadas.remove(Integer.parseInt(var)-1);                    
                }catch(Exception e){
                    System.out.println("Error:"+ e);
            }                                                                
            }
            else{
                System.out.println("El usuario será informado que su compra no fue aceptada");
                Cliente.comprasRealizadas.remove(Integer.parseInt(var)-1);
                Cliente.cotizacionesAceptadas.clear();
            }
            
        }
        else{
            System.out.println("No tiene peticiones que revisar ");
        }
        
    }
    
    /**
     * Metodo consultar Empleado recibe por teclado el nombre y apellido de un usuario
     * y muestra la informacion detallada del usuario consultado.
     * @author: Evelyn Solorzano
     */
    
    public void consultarEmpleado() throws IOException{
        System.out.println("Ingrese el nombre y el apellido del empleado");        
        String nombre = "";
        String apellido ="";
        String []l;
        while(true){
            String cadena = sc.nextLine();
            l = cadena.split(" ");
            if(l.length==2){
                break;
            }                        
        }   
        nombre = l[0];
        apellido = l[1];
        System.out.println(nombre);
        System.out.println(apellido);
        //recorrer el archivo
        List<String> listaLineas = lecturaArchivoSimple(ruta);        
        int cont=0;//contador
        for(String e:listaLineas){            
            String [] li = e.split(",");            
            if(li[1].equalsIgnoreCase(nombre)&&li[2].equalsIgnoreCase(apellido)){
                System.out.println("Tipo de Usuario: "+li[0]+"\nNombre: "+li[1]+" "+li[2]+
                    "\nUser: "+li[3]+"\nVentas: "+ li[5]);
                cont++;
            }
        }if(cont==0){
            System.out.println("No existe registro del empleado");            
        }            
        }
        
       
    public void modificarInformacionEmpleado() throws IOException{
        int option = 0;    
        List<String> lineas =ManejoArchivos.lecturaArchivoSimple("src\\Archivos\\Usuarios.txt");
        ArrayList<String> lineasGuardar = new ArrayList<String>();        
        int i = 1;
        for(String l: lineas){
            String [] partes =l.split(",");
            if(!partes[0].equals("C")){
                System.out.println("\n");
                System.out.println("Empleado: "+i);
                System.out.println("Tipo: " + partes[0]);
                System.out.println("Nombre: "+ partes[1]);
                System.out.println("Apellido: "+ partes[2]);
                System.out.println("Usuario: "+ partes[3]);
                System.out.println("Contraseña: "+ partes[4]);
            }                        
            lineasGuardar.add(l);
            i++;
        }
        option= Login.validarOpcion("Ingrese un numero para seleccionar el empleado a modificar: ");
        String linea = lineasGuardar.get(option-1);
        lineasGuardar.remove(option-1);
        System.out.println("¿Que desea modificar? \n1.Tipo \n2.Nombre \n3.Apellido \n4.Usuario \n5.Contraseña ");
        int opcionModificar= Login.validarOpcion("Ingrese un numero para seleccionar que desea modificar");
        if(opcionModificar==1){
            System.out.println("Recuerde escribir V,J,S");
            String lineaGuardar = "";
            String[] lineaCompleta = linea.split(",");
            Scanner sc = new Scanner(System.in);
            String tipo = sc.nextLine();//lo q ingresa por teclado
            tipo=tipo.trim();// trim() elimina espacios
            String linea_nueva = tipo+","+lineaCompleta[1]+","+lineaCompleta[2]+","+lineaCompleta[3]+","+lineaCompleta[4]+","+lineaCompleta[5];
            lineasGuardar.add(linea_nueva);                        
            for(String l : lineasGuardar){
                System.out.println(l);
            }
            ManejoArchivos.escribir("src\\Archivos\\Usuarios.txt", lineasGuardar);
        }
        if(opcionModificar==2){   
            System.out.println("Va a ingresar el nuevo nombre: ");
            String lineaGuardar = "";
            String[] lineaCompleta = linea.split(",");
            Scanner sc = new Scanner(System.in);
            String nombre = sc.nextLine();//lo q ingresa por teclado
            nombre=nombre.trim();// trim() elimina espacios
            String linea_nueva = lineaCompleta[0]+","+nombre+","+lineaCompleta[2]+","+lineaCompleta[3]+","+lineaCompleta[4]+","+lineaCompleta[5];
            lineasGuardar.add(linea_nueva);                        
            for(String l : lineasGuardar){
                System.out.println(l);
            }
            ManejoArchivos.escribir("src\\Archivos\\Usuarios.txt", lineasGuardar);            
        }
        if(opcionModificar==3){
            System.out.println("Va a ingresar el nuevo Apellido: ");
            String lineaGuardar = "";
            String[] lineaCompleta = linea.split(",");
            Scanner sc = new Scanner(System.in);
            String apellido = sc.nextLine();//lo q ingresa por teclado
            apellido=apellido.trim();// trim() elimina espacios
            String linea_nueva = lineaCompleta[0]+","+lineaCompleta[1]+","+apellido+","+lineaCompleta[3]+","+lineaCompleta[4]+","+lineaCompleta[5];
            lineasGuardar.add(linea_nueva);                        
            for(String l : lineasGuardar){
                System.out.println(l);
            }
            ManejoArchivos.escribir("src\\Archivos\\Usuarios.txt", lineasGuardar);                        
        }
        if(opcionModificar==4){
            System.out.println("Va a ingresar el nuevo usuario: ");
            String lineaGuardar = "";
            String[] lineaCompleta = linea.split(",");
            Scanner sc = new Scanner(System.in);
            String usuario = sc.nextLine();//lo q ingresa por teclado
            usuario=usuario.trim();// trim() elimina espacios
            String linea_nueva = lineaCompleta[0]+","+lineaCompleta[1]+","+lineaCompleta[2]+","+usuario+","+lineaCompleta[4]+","+lineaCompleta[5];
            lineasGuardar.add(linea_nueva);                        
            for(String l : lineasGuardar){
                System.out.println(l);
            }
            ManejoArchivos.escribir("src\\Archivos\\Usuarios.txt", lineasGuardar); 
        }
        if(opcionModificar==5){
            System.out.println("Va a ingresar la nueva contraseña: ");
            String lineaGuardar = "";
            String[] lineaCompleta = linea.split(",");
            Scanner sc = new Scanner(System.in);
            String contrasena = sc.nextLine();//lo q ingresa por teclado
            contrasena=contrasena.trim();// trim() elimina espacios
            String linea_nueva = lineaCompleta[0]+","+lineaCompleta[1]+","+lineaCompleta[2]+","+lineaCompleta[3]+","+contrasena+","+lineaCompleta[5];
            lineasGuardar.add(linea_nueva);                        
            for(String l : lineasGuardar){
                System.out.println(l);
            }
            ManejoArchivos.escribir("src\\Archivos\\Usuarios.txt", lineasGuardar); 
        }        
    };
    /**
    *Metodo para crear un usuario nuevo por el Supervisor mediante la seleccion de un perfil
    *@author: Evelyn Solorzano 
    */
    public void crearEmpleado() throws IOException{
        String option,profile = "";
        do{
            System.out.println("Elija el Perfil del Usuario\nVendedor: V\nJefe Taller: J");
            option = sc.next().trim();
            while(!option.equalsIgnoreCase("V")&&!option.equalsIgnoreCase("J")){
                System.out.println("Seleccione un perfil valido");
                option = sc.next().trim();
                sc.nextLine();
            }
            option= option.toUpperCase();
            sc.nextLine();
            switch(option){
                case "V":
                    profile="V";
                    break;
                case "J":
                    profile="J";
                    break;               
            }
        } while(!option.equalsIgnoreCase("V")&&
                    !option.equalsIgnoreCase("J"));
        String n=Login.validarPalabra("Ingrese el nombre: ");
        n=Character.toUpperCase(n.charAt(0)) +n.substring(1);
        String ln=Login.validarPalabra("Ingrese el apellido: ");
        ln=ln.substring(0, 1).toUpperCase() +ln.substring(1);
        System.out.println("Ingrese al usuario: ");
        Scanner sc = new Scanner(System.in);
        String u = sc.nextLine();//lo q ingresa por teclado
        u=u.trim();// trim() elimina espacios
        System.out.println("Ingrese la contraseña: ");
        Scanner sc1 = new Scanner(System.in);
        String pw = sc1.nextLine();//lo q ingresa por teclado
        pw=pw.trim();// trim() elimina espacios
        System.out.println("La contraseña es válida ");
        System.out.println("");
        String data=profile+","+n+","+ln+","+u+","+pw+","+'0';
        userRegister(data);
    }
    /**
     * Metodo userRegister recibe como argumento una cadena de caracteres con los 
     * datos del usuario a registrar en el archivo
     * @author Evelyn Solorzano
     * @param data
     */
    public void userRegister(String data) throws IOException{
        String option;
        System.out.println("Desea guardar los cambios? (Y/N): ");
        option=sc.next().trim();
        while (!option.equalsIgnoreCase("Y")&&!option.equalsIgnoreCase("N")){
            System.out.println("Error ! ");
            System.out.print("Desea guardar los cambios realizados? (Y/N): ");
            option = sc.next();
            sc.nextLine();
        }
        option= option.toUpperCase();
        sc.nextLine();
        switch(option){
            case "Y":
                userWrite(data);
                break;
            case "N":
                System.out.println("NO SE GUARDARON CAMBIOS ");
                break;
        }
    }
    
    /***
     *Metodo userWrite escribe en el archivo de Usuarios 
     * @author Evelyn Solorzano
     */
    private void userWrite(String data) throws IOException {
        List<String> lineas =ManejoArchivos.lecturaArchivoSimple("src\\Archivos\\Usuarios.txt");
        ArrayList<String> lineasGuardar = new ArrayList<String>();
        for(String l:lineas){
            lineasGuardar.add(l);
        }
        lineasGuardar.add(data);
        ManejoArchivos.escribir("src\\Archivos\\Usuarios.txt", lineasGuardar); 
        System.out.println("USUARIO CREADO CORRECTAMENTE");
        System.out.println("");
    }

    public void ingresarStock(){};
    public void consultarVReparacion(){
        if(JefeTaller.mantenimiento.size()>0){
            for(Vehiculo v:JefeTaller.mantenimiento){
                System.out.println("\n Estos son los autos que están en raparación");
                System.out.println(" Marca: "+v.getMarca()+ " Modelo: "+v.getModelo()+" Año de fabricación: " +v.getAñoFabricacion());
            }
        }
        else{
            System.out.println("\n");
            System.out.println("No hay autos en reparacion");
        }
        
    };

   

    
}
