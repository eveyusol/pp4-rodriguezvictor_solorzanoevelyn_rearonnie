package usuarios;

import concesionaria.Solicitud;
import java.util.Objects;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Victor Rodriguez
 */
public abstract class User {
    // atributos generales de los usuarios
    public String name;
    public String lastname;
    
    public String password;
    public String cuenta;
    
    
    //constructor 
    public User(String name, String lastname, String password, String Cuenta){
        this.name=name;
        this.lastname=lastname;
        
        this.password=password;
        this.cuenta=Cuenta;
        
    }
    // metodos get y set para acceder en las diferentes clases 

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCuenta() {
        return cuenta;
    }

    public void setCuenta(String cuenta) {
        this.cuenta = cuenta;
    }
    
    
    // METODO toString para poder mostrar las informacion de los diferentes usuarios
     @Override
    public String toString() {
        return "nombre=" + name + ", apellidos=" + lastname  + ", contraseña=" + password + ", correoElectronico=" + cuenta;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 31 * hash + Objects.hashCode(this.name);
        hash = 31 * hash + Objects.hashCode(this.lastname);
        hash = 31 * hash + Objects.hashCode(this.password);
        hash = 31 * hash + Objects.hashCode(this.cuenta);
        return hash;
    }

    

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final User other = (User) obj;
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.lastname, other.lastname)) {
            return false;
        }
        if (!Objects.equals(this.password, other.password)) {
            return false;
        }
        if (!Objects.equals(this.cuenta, other.cuenta)) {
            return false;
        }
        return true;
    
        }
    public abstract void revisarSolicitud();
        
    
}
