/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package usuarios;

import concesionaria.Solicitud;
import concesionaria.Stock;
import java.util.ArrayList;
import java.util.Objects;
import java.util.Scanner;
import sistema.Vehiculo;

/**
 *
 * @author eveyusol
 */
public class Vendedor extends User{
    private int numVentas;
    //el vendedor es capaz de agregar nuevos vehiculos
    private ArrayList<Vehiculo> vehiculosRegistrados;    

    public Vendedor(String name, String lastname, String password, String Cuenta) {
        super(name, lastname, password, Cuenta);
        vehiculosRegistrados = new ArrayList<>();
    }
    
    //El metodo permite ir agregando nuevos objetso de tipo vehic
     public void addVehiculo(Vehiculo vehiculo){
        vehiculosRegistrados.add(vehiculo);
    }
     
     
   //VENDEDOR ES CAPAZ DE PODER MANUPLAR CIERTOS DETALLES
    public void disminuirStock(Vehiculo p,int cantidad){
        for(Vehiculo ppr: vehiculosRegistrados){
             if(p.equals(ppr)){
                int o= ppr.getStock();
                ppr.setStock(o-cantidad);
                System.out.println("Se redujo las unidades a: "+String.valueOf(o-cantidad));   
             }
        }
     }
    public void aumentarStock(Vehiculo p,int kntidad){
         for(Vehiculo ppr: vehiculosRegistrados){
             if(p.equals(ppr)){                 
                 int o=  ppr.getStock();
                 ppr.setStock(o+kntidad);
                 System.out.println("Se aumento las unidades a: "+String.valueOf(o+kntidad));
                 
             }
         }
     }

    
    
    private void justificacionRechazo (){
        System.out.println("Lo sentimos su solicitud ha sido rechazada");        
    }
    
    //private Vehiculo solicitudAp (){
    //}

    @Override
    public void revisarSolicitud() {
        if(sistema.Sistema.listaVendedoresAleatorios.size()>0 ){
            for(Vendedor v : sistema.Sistema.listaVendedoresAleatorios){
                if(v.getCuenta().equals(this.cuenta)&&Cliente.cotizacionesRealizadas.size()>0){
                    System.out.println("Tienes solicitudes de cotizacion que revisar");
                    int i = 1;
                    for(Vehiculo ve : Cliente.cotizacionesRealizadas){
                        System.out.println(i+" Marca: "+ve.getMarca()+ " Modelo: "+ve.getModelo()+" Año de fabricación: " +ve.getAñoFabricacion());
                        i++;
                    }
                    i=1;
                    System.out.println("Elija el numero del vehiculo que desea comprar:");            
                    String var= "";
                    while(var.equals("") || Integer.parseInt(var)<0){
                        Scanner sc = new Scanner(System.in);
                        var = sc.nextLine();
                        var=var.trim();
                    }
                    //Se agregará al vehiculo cotizado 
                    Vehiculo vCotizado = Cliente.cotizacionesRealizadas.get(Integer.parseInt(var)-1);
                    System.out.println("Se acepto cotizar el vehiculo con marca:"+ vCotizado.getMarca());
                    Cliente.cotizacionesAceptadas.add(vCotizado);
                    Cliente.cotizacionesRealizadas.remove(Integer.parseInt(var)-1); //Se elimina de las cotizaciones realizadas                    
                }
                else{
                    System.out.println("No existen cotizaciones");
                }
            }                            
        }        
        else{
            if(Cliente.cotizacionesRealizadas.size()>0){
                System.out.println("Tienes cotizaciones que revisar");
                int i = 1;
                for(Vehiculo v : Cliente.cotizacionesRealizadas){
                    System.out.println(i+" Marca: "+v.getMarca()+ " Modelo: "+v.getModelo()+" Año de fabricación: " +v.getAñoFabricacion());
                    i++;
                }
                i=1;
                System.out.println("Elija el numero del vehiculo que desea aceptar la cotizacion:");            
                String var= "";
                while(var.equals("") || Integer.parseInt(var)<0){
                    Scanner sc = new Scanner(System.in);
                    var = sc.nextLine();
                    var=var.trim();
                }          
                //Se agregará al vehiculo cotizado 
                Vehiculo vCotizado = Cliente.cotizacionesRealizadas.get(Integer.parseInt(var)-1);
                System.out.println("Se acepto cotizar el vehiculo con marca:"+ vCotizado.getMarca());
                Cliente.cotizacionesAceptadas.add(vCotizado);
                Cliente.cotizacionesRealizadas.remove(Integer.parseInt(var)-1); //Se elimina de las cotizaciones realizadas                                    
            }
            else{
                System.out.println("No existen cotizaciones");
            }
        }
    }


    public void revisarStock() {
        System.out.println("Estos son los carros que se encuentran  en stock incluyendo los solicitados");
        for(Vehiculo v: Stock.modelosDisponibles){
            System.out.println(" Marca: "+v.getMarca()+ " Modelo: "+v.getModelo()+" Año de fabricación: " +v.getAñoFabricacion()+" Tipo de combustible: "+v.getTipoCombustible()+" Estado: "+v.getEstado()+" Precio: "+v.getPrecio());
        }
    }
    

    public void consultarVehiculoVendido() {
        System.out.println("Estos son los vehiculos vendidos");
        for(CompraDeVehiculo cv: Cliente.comprasAceptadas){
            Vehiculo v = cv.getVehiculo();
            System.out.println(" Marca: "+v.getMarca()+ " Modelo: "+v.getModelo()+" Año de fabricación: " +v.getAñoFabricacion()+" Tipo de combustible: "+v.getTipoCombustible()+" Estado: "+v.getEstado()+" Precio: "+v.getPrecio());
        }
    }

    
    //metodo toString

    @Override
    public String toString() {
        return "Vendedor{" + "numVentas=" + numVentas + ", vehiculosRegistrados=" + vehiculosRegistrados + '}';
    }
    
    //los atributos estan en privados por ende se usan metodos getters y setters

    public int getNumVentas() {
        return numVentas;
    }

    public void setNumVentas(int numVentas) {
        this.numVentas = numVentas;
    }

    public ArrayList<Vehiculo> getVehiculosRegistrados() {
        return vehiculosRegistrados;
    }

    public void setVehiculosRegistrados(ArrayList<Vehiculo> vehiculosRegistrados) {
        this.vehiculosRegistrados = vehiculosRegistrados;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Vendedor other = (Vendedor) obj;
        if (this.numVentas != other.numVentas) {
            return false;
        }
        if (!Objects.equals(this.vehiculosRegistrados, other.vehiculosRegistrados)) {
            return false;
        }
        return true;
    }
    
    
}
