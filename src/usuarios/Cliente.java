/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package usuarios;

import Modelo.Comparadores.MenorMayorPrecioProducto;
import acceso.Login;
import static acceso.Login.contieneParte;
import acceso.ManejoArchivos;
import concesionaria.Color;
import concesionaria.Solicitud;
import concesionaria.Stock;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;
import sistema.Auto;
import sistema.Camion;
import sistema.Motocicleta;
import sistema.Vehiculo;

/**
 *
 * @author eveyusol
 */
public class Cliente extends User implements Serializable{
    
    //atributos propios de cliente
    public static ArrayList<Vehiculo> cotizacionesRealizadas = new ArrayList<Vehiculo>();
    public static ArrayList<Vehiculo> cotizacionesAceptadas = new ArrayList<Vehiculo>();
    public static ArrayList<Vehiculo> comprasRealizadas = new ArrayList<Vehiculo>();
    public static ArrayList<CompraDeVehiculo> comprasAceptadas = new ArrayList<CompraDeVehiculo>();
    private ArrayList<Vehiculo> WishList= new ArrayList<Vehiculo>();
    private String numeroIdentifiacion;
    private String ocupacion;
    private double ingresosMensuales;
    private char tipoCliente;
    private Whislist whislist;
    
    Solicitud solicitud;    
    
    
   //Constrctor de la clase cliente que ademas hace uso del super
     public Cliente(String name, String lastname, String password, String Cuenta){
         super(name,lastname,password,Cuenta);
     }
     
    public Cliente(String name, String lastname,String Cuenta , String password, String numeroIdentificacion, String ocupacion, double ingresosMensuales, char tipoCliente) {
        super(name, lastname,Cuenta , password);
        this.numeroIdentifiacion=numeroIdentificacion;
        this.ocupacion=ocupacion;
        this.ingresosMensuales=ingresosMensuales;
        this.tipoCliente=tipoCliente;
        comprasRealizadas=new ArrayList<>();
        whislist = new Whislist();
        //this.numeroIdentifiacion= numeroIdentifiacion;
        //this.ocupacion= ocupacion;
        //this.ingresosMensuales=ingresosMensuales;
    }
    //el cliente puede que almacene vehículos en los que el cliente pueda estar interesado, pero que aún no cotiza ni compra
     public void anadiralCarrito(CompraDeVehiculo c){
        whislist.anadirAlCarrito(c);
    }
     
     
     
     
    public void anadirCompraHecha(CompraDeVehiculo c){
        comprasAceptadas.add(c);
    }
      
      
     public void quitardelCarrito(CompraDeVehiculo c){
        whislist.quitarDelCarrito(c);
    }
     
     public void verMiCarrito(){
        whislist.mostrarCarrito();
    }
    
     
     /**
      * 
      * victor Rodriguez
      * El cliente puede consultar el stock de la concesionaria.
      * SE NECESITA PERMISO PARA ACCEDER A LA INFORMACION COMPLETA.
      */
     public void consultarVehiculo(ArrayList<Vehiculo> ListaVehiculos){
        Collections.sort(ListaVehiculos, new MenorMayorPrecioProducto());
        System.out.printf("%-15s %-15s %-15s %-15s %-15s",Color.ANSI_RED+"Marca",Color.ANSI_RED+"Modelo",Color.ANSI_RED+"Stock",Color.ANSI_RED+"Año de Fabricación");
        System.out.println("\n"); 
         for (Vehiculo vehiculosOfertado : ListaVehiculos) {
            if(vehiculosOfertado instanceof Auto){
                System.out.printf("%-15s %-15s %-15s %-15s %-15s ",vehiculosOfertado.getMarca(),vehiculosOfertado.getModelo(),vehiculosOfertado.getStock(), vehiculosOfertado.getAñoFabricacion());
            
            }else if(vehiculosOfertado instanceof Motocicleta){
                System.out.printf("%-15s %-15s %-15s %-15s %-15s ",vehiculosOfertado.getMarca(),vehiculosOfertado.getModelo(),vehiculosOfertado.getStock(), vehiculosOfertado.getAñoFabricacion());

                
            }else if(vehiculosOfertado instanceof Camion){
                System.out.printf("%-15s %-15s %-15s %-15s %-15s ",vehiculosOfertado.getMarca(),vehiculosOfertado.getModelo(),vehiculosOfertado.getStock(), vehiculosOfertado.getAñoFabricacion());
                
            
              //TRACTORES  
            }else{
                
                System.out.printf("%-15s %-15s %-15s %-15s %-15s ",vehiculosOfertado.getMarca(),vehiculosOfertado.getModelo(),vehiculosOfertado.getStock(), vehiculosOfertado.getAñoFabricacion());
            }
         }
    }
     /**
      * 
      * @CONSULTAR POR MARCA (CATEGORIA)
      */
     public void consultarPorCategoria(ArrayList<User> arrayUsuarios,String categoria){
        ArrayList<Vehiculo> listaProductos = new ArrayList<>();
        for(User u: arrayUsuarios){
            if(u instanceof Vendedor){
                for(Vehiculo p: ((Vendedor) u).getVehiculosRegistrados()){
                    listaProductos.add(p);
                }
            }  
        }
        Collections.sort(listaProductos, new MenorMayorPrecioProducto());
        System.out.printf("%-40s %-30s %-30s %-30s %-30s",Color.ANSI_RED+"Cod",Color.ANSI_RED+" Nombre",Color.ANSI_RED+"Stock",Color.ANSI_RED+"Tipo");
        System.out.println("");
        int bandera=0;
        for (Vehiculo p : listaProductos) {
            if(categoria.toLowerCase().equals(p.getMarca().toLowerCase())){ 
                if(p instanceof Auto){
                        System.out.printf("%-15s %-15s %-15s %-15s %-15s ",p.getMarca(),p.getModelo(),p.getStock(), p.getAñoFabricacion());
                        System.out.println("");
                        bandera++;
                }else if(p instanceof Motocicleta){
                        System.out.printf("%-15s %-15s %-15s %-15s %-15s ",p.getMarca(),p.getModelo(),p.getStock(), p.getAñoFabricacion());
                        System.out.println("");
                        bandera++;
                    
                }else if(p instanceof Camion){
                        System.out.printf("%-15s %-15s %-15s %-15s %-15s ",p.getMarca(),p.getModelo(),p.getStock(), p.getAñoFabricacion());
                        System.out.println("");
                        bandera++;
                        //TRACTOR
                }else{
                        System.out.printf("%-15s %-15s %-15s %-15s %-15s ",p.getMarca(),p.getModelo(),p.getStock(), p.getAñoFabricacion());
                        System.out.println("");
                        bandera++;                       
                       System.out.println("");
                        bandera++;
                }
            }  
            
        }
        if(bandera==0){
           System.out.printf("%-40s %-30s %-30s %-30s %-30s",Color.ANSI_RED+"EMPTY",Color.ANSI_RED+"EMPTY",Color.ANSI_RED+"EMPTY",Color.ANSI_RED+"EMPTY",Color.ANSI_RED+"EMPTY");
           System.out.println("");
        }
        
     }   
     
     /**
      * CLIENTE PUEDE MODIFICAR SU INFORMACION
      */
  
         
     
        
    
   
    

    @Override
    public void revisarSolicitud() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void wishListView() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void solicitarMantenimiento() {
        if(comprasAceptadas.size()>0){
            System.out.println("¿Que tipo de mantenimiento desea solicitar? \n  1.Preventivo\n  2.Emergencia");
            String var= "";
            System.out.println("Elija la opción que desea realizar:");            
            while(var.equals("") || Integer.parseInt(var)<0){
                Scanner sc = new Scanner(System.in);
                var = sc.nextLine();
                var=var.trim();
            }
            
            //Preventivo
            if(var.equals("1")){
                int i =1;
                for(CompraDeVehiculo cv: comprasAceptadas){
                    Vehiculo v = cv.getVehiculo();
                    System.out.println(i+" Marca: "+v.getMarca()+ " Modelo: "+v.getModelo()+" Año de fabricación: " +v.getAñoFabricacion());
                    i++;
                }
                i=1;
                System.out.println("Elija el vehiculo que desea mantenimiento preventivo:");
                String var1="";
                while(var1.equals("") || Integer.parseInt(var1)<0){
                    Scanner sc = new Scanner(System.in);
                    var1 = sc.nextLine();
                    var1=var1.trim();
                }
                Vehiculo mantenV = comprasAceptadas.get(Integer.parseInt(var)-1).getVehiculo();
                concesionaria.Taller.preventivo.add(mantenV);
            }
            //Emergencia
            else if(var.equals("2")){
                int i =1;
                for(CompraDeVehiculo cv: comprasAceptadas){
                    Vehiculo v = cv.getVehiculo();
                    System.out.println(i+" Marca: "+v.getMarca()+ " Modelo: "+v.getModelo()+" Año de fabricación: " +v.getAñoFabricacion());
                    i++;
                }
                i=1;
                System.out.println("Elija el vehiculo que desea mantenimiento emergencia:");            
                String var2="";
                while(var2.equals("") || Integer.parseInt(var2)<0){
                    Scanner sc = new Scanner(System.in);
                    var2 = sc.nextLine();
                    var2=var2.trim();
                }
                Vehiculo mantenV = comprasAceptadas.get(Integer.parseInt(var)-1).getVehiculo();
                concesionaria.Taller.emergencia.add(mantenV);
            }
        }
        else{
            System.out.println("No tienes compras realizadas en esta consesionaria");
        }
        
    }

    public void solicitarCotizacion() {        
        if(cotizacionesAceptadas.size()>0){            
            int i = 1;
            System.out.println("Su solicitud de cotización fue aceptada\n Presentando la lista de vehiculos cotizados...");
            for(Vehiculo v: cotizacionesAceptadas){                
                System.out.println(i+" Marca: "+v.getMarca()+ " Modelo: "+v.getModelo()+" Año de fabricación: " +v.getAñoFabricacion()+" Tipo de combustible: "+v.getTipoCombustible()+" Estado: "+v.getEstado()+" Precio: "+v.getPrecio());
                i++;
            }
            i=1;
            System.out.println("Elija el numero del vehiculo que desea comprar:");            
            String var= "";
            while(var.equals("") || Integer.parseInt(var)<0){
                Scanner sc = new Scanner(System.in);
                var = sc.nextLine();
                var=var.trim();
            }          
            //Esta lista va a ser depurada en su momento
            Vehiculo v = cotizacionesAceptadas.get(Integer.parseInt(var)-1);
            System.out.println("¿Desea comprar el vehiculo? (si o no)"+ v.getMarca());
            String var1= "";
            //Validando que el ingreso sea lo correcto
            while(!(var1.equals("si") || var1.equals("no"))){
                Scanner sc = new Scanner(System.in);
                var1 = sc.nextLine();
                //System.out.println(var1);
                //System.out.println(!(var1.equals("si") || var1.equals("no")));              
            }
            if(var1.equals("si")){
                if(this.numeroIdentifiacion==null){
                    System.out.println("\n");
                    System.out.println("Antes de comprar, necesitamos datos mas especificos de su usuario.");
                    System.out.println("\n");
                    System.out.println("");                    
                    System.out.println("Ingrese su numero de identificación (cédula):"); 
                    Scanner sc = new Scanner(System.in);
                    String numIdentificacion = sc.nextLine();
                    numIdentificacion = numIdentificacion.trim();  
                    this.numeroIdentifiacion= numIdentificacion;
                    System.out.println("Ingrese su ocupación (trabajo):");
                    Scanner sc1 = new Scanner(System.in);
                    String ocupacionC = sc1.nextLine();
                    ocupacionC = ocupacionC.trim();                    
                    this.ocupacion=ocupacionC;
                    System.out.println("Digite la cantidad de ingresos al mes que tiene ):");
                    Scanner sc2 = new Scanner(System.in);
                    String ingresosMC = sc2.nextLine();
                    ingresosMC = ingresosMC.trim();                    
                    this.ingresosMensuales=Double.parseDouble(ingresosMC);
                    System.out.println("Su compra será solicitada al supervisor, el que la aceptará cuando sea revisada.");
                    comprasRealizadas.add(v);                    
                    //Se hace todo esta linea de codigo para que los modelos que han sidos solicitados no aparezcan a otro cliente
                    int posicion=0; //Esto nos dirá la posicion en el stock
                    for(Vehiculo ve: Stock.modelosDisponibles){
                        if(ve.getModelo().equals(v.getModelo())){
                            break;
                        }
                        posicion++;
                    }
                    Vehiculo veh = Stock.modelosDisponibles.get(posicion);
                    veh.setSolicitado(true);
                    Stock.modelosDisponibles.add(veh);
                    cotizacionesAceptadas.remove(Integer.parseInt(var)-1);
                }
                else{
                    System.out.println("Su compra será solicitada al supervisor, el que la aceptará cuando sea revisada.");
                    comprasRealizadas.add(v);
                }
            }
            else{
                System.out.println("La cotización será cancelada");
                System.out.println("Se borrará la cotización");
                //Se borra la cotización
                cotizacionesAceptadas.remove(Integer.parseInt(var)-1);
            }            
        }
        else{
            if(comprasRealizadas.size()==0 && comprasAceptadas.size()==0 ){
                System.out.println("Su compra no fue aceptada por el supervisor");
                System.out.println("La razón de no haber aceptado es "+ "falta de fondos");                
            }
            consultarVehiculoDisponible();
            System.out.println("Elija el numero del vehiculo que desea cotizar:");            
            String var= "";
            while(var.equals("") || Integer.parseInt(var)<0){
                Scanner sc = new Scanner(System.in);
                var = sc.nextLine();
                var=var.trim();
            }
            Vehiculo v = Stock.modelosDisponibles.get(Integer.parseInt(var)-1);
            cotizacionesRealizadas.add(v);
            System.out.println("Eligió el vehiculo:"+ v.getMarca());
            System.out.println("Su petición será agendada a un vendedor el cual le enviará la cotización");
            //Aqui va el aleatorio para elegir al vendedor que aceptará la cotización
            if(sistema.Sistema.listaVendedores.size()>0){
                //#aleatorio 
                int numero = (int) (Math.random() * sistema.Sistema.listaVendedores.size());
                Vendedor vend = sistema.Sistema.listaVendedores.get(numero);
                sistema.Sistema.listaVendedoresAleatorios.add(vend);                
            } 
        }
    }

    public void consultarVehiculoDisponible(){
        System.out.println("Estos son los modelos disponibles:");
        int i = 1;
        for(Vehiculo v :Stock.modelosDisponibles){
            if(!v.isSolicitado()){
                System.out.println(i+" Marca: "+v.getMarca()+ " Modelo: "+v.getModelo()+" Año de fabricación: " +v.getAñoFabricacion());
            }            
            i++;
        }
        i = 1;
        
    }

    public void modificarInformacion() throws IOException {
        int option = 0;    
        List<String> lineas =ManejoArchivos.lecturaArchivoSimple("src\\Archivos\\Usuarios.txt");
        System.out.println("1.Cambiar el nombre\n2.Cambiar el apellido"+
                    "\n3.Cambiar la contraseña\n4.SALIR\n");
        System.out.println(this.name);
        option= Login.validarOpcion("Ingrese un numero para seleccionar una opcion: ");       
        ArrayList<String> lineasGuardar = new ArrayList<String>();        
        switch(option){
            case 1:
                for(String linea: lineas){
                    String[] partes = linea.split(",");
                    String linea_nueva= "";
                    String nombre="";
                    if(partes[1].equals(this.name)){
                        System.out.println("Ingrese el nuevo nombre: ");
                        Scanner sc = new Scanner(System.in);
                        nombre = sc.nextLine();//lo q ingresa por teclado
                        nombre=nombre.trim();// trim() elimina espacios
                        linea_nueva = partes[0]+","+nombre+","+partes[2]+","+partes[3]+","+partes[4]+","+partes[5];
                        this.name=nombre;
                        lineasGuardar.add(linea_nueva);
                        continue;
                    }
                    lineasGuardar.add(linea);                    
                }
                for(String l : lineasGuardar){
                    System.out.println(l);
                }
                ManejoArchivos.escribir("src\\Archivos\\Usuarios.txt", lineasGuardar);
                break;
            case 2:
                for(String linea: lineas){
                    String[] partes = linea.split(",");
                    String linea_nueva= "";
                    String apellido="";
                    if(partes[2].equals(this.lastname)){
                        System.out.println("Ingrese el nuevo apellido: ");
                        Scanner sc = new Scanner(System.in);
                        apellido = sc.nextLine();//lo q ingresa por teclado
                        apellido=apellido.trim();// trim() elimina espacios
                        linea_nueva = partes[0]+","+partes[1]+","+apellido+","+partes[3]+","+partes[4]+","+partes[5];
                        this.lastname=apellido;
                        lineasGuardar.add(linea_nueva);
                        continue;
                    }
                    lineasGuardar.add(linea);                    
                }
                for(String l : lineasGuardar){
                    System.out.println(l);
                }
                ManejoArchivos.escribir("src\\Archivos\\Usuarios.txt", lineasGuardar);
                break;
            case 3:
                System.out.println(this.cuenta);
                for(String linea: lineas){
                    String[] partes = linea.split(",");
                    String linea_nueva= "";
                    String contrasena="";
                    if(partes[4].equals(this.cuenta)){
                        System.out.println("Ingrese la nueva contraseña: ");
                        Scanner sc = new Scanner(System.in);
                        contrasena = sc.nextLine();//lo q ingresa por teclado
                        contrasena=contrasena.trim();// trim() elimina espacios
                        //Login.validarContrasenia(contrasena);
                        linea_nueva = partes[0]+","+partes[1]+","+partes[2]+","+partes[3]+","+contrasena+","+partes[5];
                        this.password=contrasena;
                        lineasGuardar.add(linea_nueva);
                        continue;
                    }
                    lineasGuardar.add(linea);                    
                }
                for(String l : lineasGuardar){
                    System.out.println(l);
                }
                ManejoArchivos.escribir("src\\Archivos\\Usuarios.txt", lineasGuardar);
                break;
            case 4:                       
                System.out.println("SALIO DEL MENU CLIENTE");        

    }
    }    

    public void definirMiWishList() throws IOException {
            consultarVehiculoDisponible();
            System.out.println("Elija el numero del vehiculo que desea agregar a su WhishList:");            
            String var= "";
            while(var.equals("") || Integer.parseInt(var)<0){
                Scanner sc = new Scanner(System.in);
                var = sc.nextLine();
                var=var.trim();
            }
            Vehiculo v = Stock.modelosDisponibles.get(Integer.parseInt(var)-1);
            WishList.add(v);
            System.out.println("Eligio el vehiculo, con marca:"+ v.getMarca());
            System.out.println("Será agregado a la WishList...");
        
    }
    
    public void setWishList(ArrayList<Vehiculo> WishList) {
        this.WishList = WishList;
    }

    public ArrayList<Vehiculo> getWishList() {
        return WishList;
    }
    
       
     
    
    

   
    
    
}
