/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package usuarios;

import concesionaria.Solicitud;
import concesionaria.Taller;
import java.util.ArrayList;
import java.util.Scanner;
import sistema.Vehiculo;

/**
 *
 * @author eveyusol
 */
public class JefeTaller extends User{
    public static ArrayList<Vehiculo> mantenimiento = new ArrayList();
    public static ArrayList<Vehiculo> entregar = new ArrayList();

    public JefeTaller(String name, String lastname, String password, String Cuenta) {
        super(name, lastname, password, Cuenta);

    }
    
    private Vehiculo entregarVehiculo (Solicitud solicitud, Vehiculo v){
        solicitud.getEstado();
        return v;

    }
    

   

    @Override
    public void revisarSolicitud() {
        if(concesionaria.Taller.emergencia.size()>0){
            System.out.println("Estos son las solicitudes de mantenimiento emergencia:");
            int i = 1;
            for(Vehiculo v: concesionaria.Taller.emergencia){
                System.out.println(i+" Marca: "+v.getMarca()+ " Modelo: "+v.getModelo()+" Año de fabricación: " +v.getAñoFabricacion());
                i++;
            }
            i=1;
            System.out.println("Elija el vehiculo que desea aceptar el mantenimiento:");
            String var1="";
            while(var1.equals("") || Integer.parseInt(var1)<0){
                Scanner sc = new Scanner(System.in);
                var1 = sc.nextLine();
                var1=var1.trim();
            }
            Vehiculo v = concesionaria.Taller.emergencia.get(Integer.parseInt(var1)-1);
            mantenimiento.add(v);
        }
        else{
            System.out.println("No existen vehiculos que estén en mantenimiento");
        }
            
              
        
        if(concesionaria.Taller.preventivo.size()>0){
            System.out.println("Estos son las solicitudes de mantenimiento preventivo:");
            int i = 1;
            for(Vehiculo v: concesionaria.Taller.preventivo){
                System.out.println(i+" Marca: "+v.getMarca()+ " Modelo: "+v.getModelo()+" Año de fabricación: " +v.getAñoFabricacion());
                i++;
            }
            i=1;
            System.out.println("Elija el vehiculo que desea aceptar el mantenimiento:");
            String var1="";
            while(var1.equals("") || Integer.parseInt(var1)<0){
                Scanner sc = new Scanner(System.in);
                var1 = sc.nextLine();
                var1=var1.trim();
            }
            Vehiculo v = concesionaria.Taller.preventivo.get(Integer.parseInt(var1)-1);
            mantenimiento.add(v);
        }                            
    }

    public void registroInfoMant() {
        if(mantenimiento.size()>0){
            for(Vehiculo v: mantenimiento){
                System.out.println(" Marca: "+v.getMarca()+ " Modelo: "+v.getModelo()+" Año de fabricación: " +v.getAñoFabricacion());
            }
            System.out.println("Elija el vehiculo que ya ha culminado su mantenimiento:");
            String var1="";
            while(var1.equals("") || Integer.parseInt(var1)<0){
                Scanner sc = new Scanner(System.in);
                var1 = sc.nextLine();
                var1=var1.trim();
            }
            Vehiculo v = mantenimiento.get(Integer.parseInt(var1)-1);
            entregar.add(v);
            mantenimiento.remove(Integer.parseInt(var1)-1);
            System.out.println("El carro esta listo para entregar");
        }else{
            System.out.println("No se puede presentar ningún dato ya que no existe ningún carro en mantenimiento");
        }
    }

    public void vehiculoMantenimiento() {
        System.out.println("Estos vehiculos se encuentran en mantenimiento: ");
        if(mantenimiento.size()>0){
            for(Vehiculo v: mantenimiento){
                System.out.println(" Marca: "+v.getMarca()+ " Modelo: "+v.getModelo()+" Año de fabricación: " +v.getAñoFabricacion());
            }                  
        }
    }

    public void entregarVehiculo() {
        if(Cliente.comprasAceptadas.size()>0){
            System.out.println("Estos son los carros que deben ser entregados, ya que fue aceptada la compra por el supervisor");
        for(CompraDeVehiculo cv:Cliente.comprasAceptadas){
            Vehiculo v = cv.getVehiculo();
            System.out.println(" Marca: "+v.getMarca()+ " Modelo: "+v.getModelo()+" Año de fabricación: " +v.getAñoFabricacion());
        }
        System.out.println("\n");
        }
        else{
            System.out.println("No hay carros que mostrar ");
        }
        
        
        if(entregar.size()>0){
            System.out.println("Estos carros pasaron por mantenimiento y deben ser entregados");
            for(Vehiculo ve: entregar){
                System.out.println(" Marca: "+ve.getMarca()+ " Modelo: "+ve.getModelo()+" Año de fabricación: " +ve.getAñoFabricacion());
            }            
        }        
    }

    
}
