
package acceso;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author eveyusol
 */
public class Login {
   
    static Scanner sc = new Scanner(System.in);
  
    public Login (){
    }

    public static String textoCualquiera(String cadena) throws IOException{
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in)); //Ya tenemos el "lector"

        System.out.print(cadena);//Se pide un dato al usuario

        String texto = br.readLine(); 
        br.close();
        return Character.toUpperCase(texto.charAt(0)) + texto.substring(1);
        
    }
    
    public static boolean esPalabraCaracteres(String palabra){ 
        for(int i = 0; i < palabra.length(); i++){ 
            if(!((palabra.charAt(i) > 64 && palabra.charAt(i) < 91) || 
                (palabra.charAt(i) > 96 && palabra.charAt(i) < 123)||palabra.charAt(i)==32||
                    //la ñ Ñ
                    palabra.charAt(i)==164||palabra.charAt(i)==165
                    //letras con tildes
                ||(palabra.charAt(i)>=160 &&palabra.charAt(i) <=163||palabra.charAt(i)==130)
                ||palabra.charAt(i)==181||palabra.charAt(i) ==144||palabra.charAt(i)==214||palabra.charAt(i) ==224||palabra.charAt(i)==233)) 
            return false; 
        } 
        return true; 
    } //Nota: funciona solamente para palabras sin espacios y de la a-z ó A-Z 
    public static String validarPalabra(String cadena){
        System.out.print(cadena);
        String var = sc.nextLine();//lo q ingresa por teclado
        var=var.trim();
        while(!esPalabraCaracteres(var)||var.equals("")){
            System.out.println("Lo siento, la palabra contiene caracteres no validos o no ingreso nada");
            System.out.print(cadena);
            var = sc.nextLine();
            var=var.trim();
            
        }
        //var.substring(0, 1).toUpperCase() + var.substring(1)
        //Character.toUpperCase(var.charAt(0)) + var.substring(1)
        return var;
    }
    public static boolean esCaracteresUserValido(String palabra){ 
        for(int i = 0; i < palabra.length(); i++){ 
            if(!((palabra.charAt(i) > 96 && palabra.charAt(i) < 123)||//no debe haber espacios
                    //la ñ
                    palabra.charAt(i)==164
                    //letras con tildes en minuscula
                ||(palabra.charAt(i)>=160 &&palabra.charAt(i) <=163||palabra.charAt(i)==130))) 
            return false; 
        } 
        return true; 
    }
    public static  String validarUsuario(String cadena) throws IOException{
        System.out.print(cadena);
        String var = sc.nextLine();//lo q ingresa por teclado
        var=var.trim();
        while(esCaracteresUserValido(var)||var.equals("")){
            
            //System.out.println(esCaracteresUserValido(var));
            //System.out.println(var.equals(""));
            
            //Añadiendo validacion: El usuario debe estar en el txt
            List<String> lineas =ManejoArchivos.lecturaArchivoSimple("src\\Archivos\\Usuarios.txt");
            for(String linea:lineas){
                String[] partes = linea.split(",");
                String compUsuario = partes[3];
                //System.out.println(compUsuario);
                if(compUsuario.equals(var)){
                    return var;                    
                }
            }
            /*if(var.equals("")){System.out.println("no ingreso nada");}
            else{
                System.out.println("Lo siento, la palabra contiene caracteres no validos como"
                        + " espacios o guiones "+"\n"+ "Solo puede ingresar letras en minusculas");
            }*/
            System.out.println("El usuario es incorrecto, digite un usuario existente");
            System.out.print(cadena);
            var = sc.nextLine();
            var=var.trim();            
        }
        System.out.println("El usuario es incorrecto, digite un usuario existente");
        return null;
        
    }
    public static  String validarContrasenia(String cadena){
        String contrasenia;
        boolean cambiar=true;
        //contrasenia.charAt(0) es la primera letra
        //contrasenia.charAt(0) > 64 && contrasenia.charAt(0) < 91    
        //Character.isUpperCase() devuelve un boolean, true si la letra es mayuscula
        //Character.isUpperCase(contrasenia.charAt(0))
        do{
            System.out.print(cadena);
            contrasenia = sc.nextLine();
            contrasenia=contrasenia.trim();
            if(!(contrasenia.equals("")||contrasenia.equals(" "))){
                if((Character.isUpperCase(contrasenia.charAt(0)))&&(contrasenia.length()>8 && contrasenia.length()<13)){
                    cambiar=false;
                }else{
                    System.out.println("La contraseña no es válida, debe empezar con mayuscula y tener de 8 a 12 caracteres");  
                }

            }else{
                System.out.println("No ingreso nada");
                System.out.print(cadena);
                contrasenia = sc.nextLine();
                contrasenia=contrasenia.trim();
            }
        }while(cambiar);
            
        
        
        System.out.print("\n");
        return contrasenia;
    }
  
   
    public double getInfornation(String chain){
        String var="";
        double inf; 
        while((var.equals("")||var.matches("[A-Za-z1-9 ]*$"))){
            System.out.println(chain);
            var = sc.nextLine();
        }
        inf = Double.parseDouble(var);
        return inf;
    }
    /*Metodo para cerrar el Scanner*/
    public void scannerClose(){
        sc.close();
    }
    
    /*
    Metodo para validar el ingreso de opciones al menu 
    */
    public static boolean isNumberCharacter(String c){ 
        for(int i = 0; i <c.length(); i++){ 
            if(!Character.isDigit(c.charAt(i))) 
            return false; 
        } 
        return true; 
    }
    
    public static int validarOpcion(String op){
        int option;
        System.out.print(op);
        String line = sc.nextLine();//lo q ingresa por teclado
        line=line.trim();// trim() elimina espacios
        while(!isNumberCharacter(line)||line.equals("")||line.length()>1){
            System.out.println("SO SORRY\nIngresa una opcion valida");
            System.out.print(op);
            line = sc.nextLine();
            line=line.trim();
            
        }
        //var.substring(0, 1).toUpperCase() + var.substring(1)
        return option= Integer.parseInt(line);    
    }
    
    
    // PRUEBA DE UNA VALIDACION 
    public static boolean contieneParte(String parte, String contenedor){
        char[] Arrparte = parte.toLowerCase().toCharArray();
        char[] Arrcont= contenedor.toLowerCase().toCharArray();
        if(Arrcont.length> Arrparte.length){
            for(int i=0;i<Arrparte.length;i++){
                if(!(Arrcont[i]==Arrparte[i])){
                          return false;}
            } 
        }else if(Arrcont.length< Arrparte.length){
            for(int i=0;i<Arrcont.length;i++){
                if(!(Arrcont[i]==Arrparte[i])){
                          return false;}
            } 
        }else{
            for(int i=0;i<Arrparte.length;i++){
                if(!(Arrcont[i]==Arrparte[i])){
                          return false;}}
        }
         
        return true;
    }
}
