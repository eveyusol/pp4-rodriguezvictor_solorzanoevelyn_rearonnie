/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package acceso;
import static acceso.Menu.imprimeEncabezado;
import usuarios.JefeTaller;

/**
 *
 * @author Evelyn Solorzano
 */
class MenuJefeTaller {
    
    static JefeTaller jefetaller;    
    static void mostrarMenu() {
        int option = 0;
        jefetaller = (JefeTaller) Menu.u;
        do{
            imprimeEncabezado("JEFE DE TALLER");
            System.out.println("1.Entregar Vehiculo\n2.Vehiculos en Mantenimiento"+
                    "\n3.Revisar Solicitudes de Mantenimiento\n4.Registrar Informacion de Mantenimiento"+
                    "\n5.SALIR");
            option= Login.validarOpcion("Ingrese una opcion: ");
            switch(option){
                case 1:
                    jefetaller.entregarVehiculo();
                    break;
                case 2:
                    jefetaller.vehiculoMantenimiento();
                    break;
                case 3:
                    jefetaller.revisarSolicitud();
                    break;
                case 4:     
                    jefetaller.registroInfoMant();
                    break;
                case 5:
                    System.out.println("SALIO DEL MENU JEFE DE TALLER");
            }
        
        } while(option!=5);
        
    }
}
