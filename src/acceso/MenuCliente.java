/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package acceso;

import static acceso.Menu.imprimeEncabezado;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Scanner;
import sistema.Vehiculo;
import usuarios.Cliente;
import usuarios.CompraDeVehiculo;
import usuarios.JefeTaller;
import usuarios.Supervisor;
import usuarios.User;

/**
 *
 * @author 59395
 */
public class MenuCliente {
    static Scanner scan=Login.sc;
    protected static final String ruta="src\\Archivos\\Usuarios.txt";
    static Cliente cliente;

    public static void mostrarMenu() throws IOException {
        int option = 0;
        if(usuarios.Cliente.comprasAceptadas.size()>0){
            System.out.println("Su compra fue aceptada con exito por supervisor");
            System.out.println("Actualmente cuenta con estos carros comprados: ");
            System.out.println("Será entregado por el jefe de taller, el día que usted desee");
            System.out.println("Lo tendremos en las mejores condiciones");
            System.out.println("Gracias por su compra");
        }        
        do{
            cliente = (Cliente) Menu.u;
            imprimeEncabezado("CLIENTE");
            System.out.println("1. Modificar Datos Personales\n2.Consultar Vehiculos Disponibles"+
                    "\n3.Solicitar Cotizacion\n4.Solicitar Mantenimiento Preventido o de Emergencia"+
                    "\n5.Definir mi WishList"+
                    "\n6.Mostrar WishList\n7.SALIR\n");
            if(usuarios.Cliente.comprasAceptadas.size()>0){    
                System.out.println("Estos son los carros comprados");
                for(CompraDeVehiculo cv : usuarios.Cliente.comprasAceptadas){
                    System.out.println(" Marca: "+cv.getVehiculo().getMarca()+ " Modelo: "+cv.getVehiculo().getModelo()+" Año de fabricación: " +cv.getVehiculo().getAñoFabricacion());
                }                
            }
            option= Login.validarOpcion("Ingrese un numero para seleccionar una opcion: ");
            switch(option){
                case 1:
                    System.out.println(" \n Se modificará la información \n");
                    cliente.modificarInformacion(); 
                    break;
                case 2:
                    cliente.consultarVehiculoDisponible();
                    break;
                case 3:
                    cliente.solicitarCotizacion();
                    break;
                case 4:     
                    cliente.solicitarMantenimiento();
                    break;
                case 5:     
                    cliente.definirMiWishList();
                    break;
                case 6:
                    for (Vehiculo v : cliente.getWishList()) {
                        System.out.println(" Marca: "+v.getMarca()+ " Modelo: "+v.getModelo()+" Año de fabricación: " +v.getAñoFabricacion());
                    }
                    break;                    
                case 7:
                    System.out.println("SALIO DEL MENU CLIENTE");
            }        
        } while(option!=7);
        
    }

}
