/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package acceso;

import java.io.IOException;
import java.util.List;
import usuarios.Cliente;
import usuarios.JefeTaller;
import usuarios.Supervisor;
import usuarios.User;
import usuarios.Vendedor;



/**
 *
 * @author eveyusol
 */
public class Menu {
    static User u;
    public static void MenuInicial() throws IOException{
    imprimeEncabezado("AL SISTEMA");
        
        do{
            //String usuario=Login.validarPalabra("USUARIO: ");
            String usuario = null;
            //Validando el usuario correctamente
            while(usuario == null){
                usuario = Login.validarUsuario("USUARIO: ");
            }
            //System.out.println("Hola jejeje" + usuario+ "adfasf");
            String contrasenia=Login.validarContrasenia("CONTRASEÑA: ");
            u=BuscarUsuarioEnArchivo(usuario,contrasenia,"src\\Archivos\\Usuarios.txt");
        }while(u==null);        
        //Para que vuela a poder iniciar sesión y poder revisar si se cumple que se estableció un vendedor        
        elijeMenu(u);
        u=null;
        MenuInicial();
    }

        //nombre+","+apellido+","+usuario+","+contrasenia+","+perfil   
    static User BuscarUsuarioEnArchivo(String usuario,String contrasenia,String Filename) throws IOException{        
        List<String> lineas =ManejoArchivos.lecturaArchivoSimple(Filename);
        for(String linea:lineas){
            String[] partes = linea.split(",");
            String parte1 = partes[0];
            String parte2 = partes[1];
            String parte3 = partes[2];
            String parte4=partes[3];
            String parte5=partes[4];
            if(parte4.equals(usuario)&&parte5.equals(contrasenia)){               
                switch(parte1){
                    case "S": //Supervisor
                        u=new Supervisor(parte2,parte3,parte4,parte5);
                        sistema.Sistema.listaSupervisor.add((Supervisor)u);
                        return u;
                    case "V": //Vendedor
                        u=new Vendedor(parte2,parte3,parte4,parte5);
                        sistema.Sistema.listaVendedores.add((Vendedor)u);
                        return u;
                    case "J": //Jefe del taller
                        u=new JefeTaller(parte2,parte3,parte4,parte5);
                        sistema.Sistema.listaJefeTaller.add((JefeTaller)u);
                        return u;
                    case "C": //Cliente
                        u=new Cliente(parte2,parte3,parte4,parte5);
                        sistema.Sistema.listaCliente.add((Cliente)u);
                        return u;
                }
            }        
        }
        System.out.println("Lo sentimos ese usuario no existe en el sistema o la contraseña no era válida .");
        return null;
    }
    
    static void elijeMenu(User u) throws IOException{
        if(u instanceof Cliente){            
            MenuCliente.mostrarMenu();
        }
         if(u instanceof JefeTaller){
            MenuJefeTaller.mostrarMenu();
        }
        if(u instanceof Supervisor){
            MenuSupervisor.mostrarMenu((Supervisor)u);
        }
        if(u instanceof Vendedor){
            MenuVendedor.mostrarMenu();
            
        }
     
    }
    protected static void imprimeEncabezado(String user){   
        for(int i=0;i<3;i++){
            if(i==1){
                System.out.println("        BIENVENIDO "+user);
                System.out.println("\n");
            }else{

                for(int j=0;j<35;j++){
                    System.out.print("-");                   
                }System.out.println("\n");
            }
        }
    }

    
    /*random de vendedor()
    
        solitudes()
    */
}
