
 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package acceso;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.RandomAccessFile;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;



public class ManejoArchivos {
  
    /*Usando NIO*/
    public static List<String> lecturaArchivoSimple(String inFilename) throws IOException{
        Charset charset = Charset.forName("ISO-8859-1");
        Path path = Paths.get(inFilename);
        return Files.readAllLines(path);
        
    }

    public static void escribir(String ruta,  List<String> lineas ) {
        File f;
        f = new File(ruta);
        //Escritura
        try{
            FileWriter w = new FileWriter(f);
            BufferedWriter bw = new BufferedWriter(w);
            PrintWriter wr = new PrintWriter(bw);  
            wr.write("");//escribimos en el archivo algo vacio para que se borre los datos
            for(String linea: lineas ){
                wr.append(linea+"\n"); //concatenamos en el archivo sin borrar lo existente                
            }            
            //ahora cerramos los flujos de canales de datos, al cerrarlos el archivo quedará guardado con información escrita                   //de no hacerlo no se escribirá nada en el archivo
            wr.close();
            bw.close();
}catch(IOException e){};
}

    

   
    
    public static void read(String fileName) throws FileNotFoundException, IOException{
        File archivo = new File (fileName);
        FileReader fr = new FileReader (archivo);
        BufferedReader br = new BufferedReader(fr);
        String line = br.readLine();
    }

    
    
}
    

    
