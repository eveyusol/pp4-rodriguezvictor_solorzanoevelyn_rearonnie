/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package acceso;
import java.io.IOException;
import usuarios.Supervisor;

/**
 *
 * @author Solorzano Evelyn
 */
public class MenuSupervisor extends Menu {
    

    public static void mostrarMenu(Supervisor supervisor) throws IOException {
        int option;
        do{
            imprimeEncabezado("SUPERVISOR");
            System.out.println("1. Consultar Emplelado\n2.Modificar Informacion de Empleado"+
                    "\n3.Crear Perfil de Empleado\n4.Consultar Vehiculos en reparacion"+
                    "\n5.Revisar Solicitudes\n7.SALIR");
            option= Login.validarOpcion("Ingrese un numero para seleccionar una opcion: ");
            switch(option){
                case 1:
                    supervisor.consultarEmpleado(); 
                    break;
                case 2:
                    supervisor.modificarInformacionEmpleado();
                    break;
                case 3:
                    supervisor.crearEmpleado();
                    break;
                case 4:     
                    supervisor.consultarVReparacion();
                    break;
                case 5:
                    supervisor.revisarSolicitud();
                    break;
                case 7:
                    System.out.println("SALIO DEL MENU SUPERVISOR");
            }
        
        } while(option!=7);
        
    }
    
        
}
