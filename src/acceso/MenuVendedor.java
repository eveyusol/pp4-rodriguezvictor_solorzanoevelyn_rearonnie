/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package acceso;


import static acceso.Menu.imprimeEncabezado;

import usuarios.Vendedor;


/**
 *
 * @author Evelyn Solorzano
 */
class MenuVendedor {

    static Vendedor seller;

    static void mostrarMenu() {
        int option = 0;
        //Downcasting requerido para acceder a los metodos
        seller = (Vendedor) Menu.u;
        do{
            imprimeEncabezado("VENDEDOR");
            System.out.println("1.Revisar Stock\n2.Revisar Solicitudes de Clientes"+
                    "\n3.Consultar Vehiculos Vendidos\n4.SALIR");
            option= Login.validarOpcion("Ingrese un numero para seleccionar una opcion: ");
            switch(option){
                case 1:
                    seller.revisarStock();
                    break;
                case 2:
                    seller.revisarSolicitud();                    
                    break;
                case 3:
                    seller.consultarVehiculoVendido();
                    break;
                case 4:     
                    System.out.println("SALIO DEL MENU VENDEDOR");
                    break;                                    
            }
        
        } while(option!=4);
      
    }

}
