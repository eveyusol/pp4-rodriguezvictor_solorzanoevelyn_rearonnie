/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo.Comparadores;


import java.util.Comparator;
import usuarios.CompraDeVehiculo;

/**
 *
 * @author victor
 */

public class MayorMenorFecha implements Comparator<CompraDeVehiculo> {
    @Override
    public int compare(CompraDeVehiculo c1, CompraDeVehiculo c2) {
        if(c1.getFechaCompra().isBefore(c2.getFechaCompra())){
            return -1;
        }else if(c1.getFechaCompra().isAfter(c2.getFechaCompra())){
            return 1;
        }        
        return 0;
    }
}