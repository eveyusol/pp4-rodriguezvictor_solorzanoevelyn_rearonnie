/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo.Comparadores;


import java.util.Comparator;
import sistema.Vehiculo;

/**
 *
 * @author victor
 */
public class MenorMayorPrecioProducto implements Comparator<Vehiculo> {
    
     @Override
    public int compare(Vehiculo p1, Vehiculo p2) {
        if(p1.getPrecio()> p2.getPrecio()){
            return 1;
        }else if(p1.getPrecio() < p2.getPrecio()){
            return -1;
        }        
        return 0;
    }
    
    
}
