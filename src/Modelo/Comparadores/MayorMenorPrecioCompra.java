/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo.Comparadores;


import java.util.Comparator;
import usuarios.CompraDeVehiculo;

/**
 *
 * @author victor
 */



public class MayorMenorPrecioCompra implements Comparator<CompraDeVehiculo>{

    @Override
    public int compare(CompraDeVehiculo p1, CompraDeVehiculo p2) {
        if(p1.getPrecioCompraDeVehiculo() > p2.getPrecioCompraDeVehiculo()){
            return -1;
        }else if(p1.getPrecioCompraDeVehiculo() < p2.getPrecioCompraDeVehiculo()){
            return 1;
        }        
        return 0;
    }
    
}
