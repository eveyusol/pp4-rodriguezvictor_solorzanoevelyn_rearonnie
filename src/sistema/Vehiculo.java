package sistema;

import concesionaria.Color;
import concesionaria.Solicitud;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author eveyusol
 */
public abstract class Vehiculo {
    
 //atributos de la clase vehiculos que es lo que sistema venden
    // recordar la categoria es la marca del vehiculo, por lo tanto no es necesario tener un atributo categoria
    private static String[] Marcas= {""};
    private static List<String> listaMarcas = Arrays.asList(Marcas);
    protected String marca;
    protected  String modelo;
    protected String añoFabricacion;
    protected double precio;    
    protected String estado;
    protected int numLlantas;
    //stock es la cantidad de vehiculos
    protected int stock;
    protected static final TipoCombustible tipoPredeterminado = TipoCombustible.GASOLINA ;
    protected final TipoCombustible tipoCombustible;
    protected boolean solicitado= false;

    public Vehiculo(String marca, String modelo, String añoFabricacion, double precio, String estado, int numLlantas, int stock, TipoCombustible tipoCombustible) {
        this.marca = marca;
        this.modelo = modelo;
        this.añoFabricacion = añoFabricacion;
        this.precio = precio;        
        this.estado = estado;
        this.numLlantas = numLlantas;
        this.stock=stock;
        this.tipoCombustible=tipoCombustible;
    }
    //verificar si el vehiculo sigue en stock
    //RECUERDA CAMBIAR EL NOMBRE DEL METODO A UN VERBO OJO
    public boolean hayStock(int cantidad) {
        if((this.stock - cantidad)<0){
            java.lang.System.out.println();
            java.lang.System.out.println(Color.ANSI_RED + "No hay stock");
            java.lang.System.out.println("");
            java.lang.System.out.println(Color.ANSI_RED + "Solo existen "+ String.valueOf(this.stock)+" unidades disponibles");
            return false;
        }
        java.lang.System.out.println();
        java.lang.System.out.println("Si hay stock\n");
        return true;
        
    }
    
    
    //Metodo estatico verifica si la categoria del tipo de vehiculo existe 
    public static Boolean verificarCategoriaExiste(String CategoriaIngresada){
        if(!listaMarcas.contains(CategoriaIngresada.toLowerCase())){
            System.out.println("\nIngrese una categoria existente: "+ "\nLas Categorias Disponibles son:");
            for(int i=0;i< listaMarcas.size();i++){
                System.out.println(listaMarcas.get(i));
            }
            
            return false;
        }
        return true;
    }
    //metodos getters y setters
    
    
    //protected void comprarVehiculo(Solicitud solicitud){}
    //protected void descartarVehiculo(Solicitud solicitud){}

    public static String[] getCategorias() {
        return Marcas;
    }

    public static void setCategorias(String[] Categorias) {
        Vehiculo.Marcas = Categorias;
    }

    public static List<String> getListaCategorias() {
        return listaMarcas;
    }

    public static void setListaCategorias(List<String> listaCategorias) {
        Vehiculo.listaMarcas = listaCategorias;
    }
    

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getAñoFabricacion() {
        return añoFabricacion;
    }

    public void setAñoFabricacion(String añoFabricacion) {
        this.añoFabricacion = añoFabricacion;
    }

    public double getPrecio() {
        return precio;
    }

    public void setSolicitado(boolean solicitado) {
        this.solicitado = solicitado;
    }

    public boolean isSolicitado() {
        return solicitado;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }
    

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public int getNumLlantas() {
        return numLlantas;
    }

    public void setNumLlantas(int numLlantas) {
        this.numLlantas = numLlantas;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public TipoCombustible getTipoCombustible() {
        return tipoCombustible;
    }

   /* public void setTipoCombustible(TipoCombustible tipoCombustible) {
        this.tipoCombustible = tipoCombustible;
    }*/
    
    
    // metodo toString para la clase vehiculo
     @Override
    public String toString() {
        return "stock="+stock + ", marca=" + marca + ", modelo=" + modelo + ", año de fabricacion=" + añoFabricacion + ", precio=" +precio+
                ", estado="+estado + ", numero de llantas=" + numLlantas+ ", tipo de combustible=" + tipoCombustible;
    }
    

    // metodo equal para comparar de manera logica
     @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Vehiculo other = (Vehiculo) obj;
        if (Double.doubleToLongBits(this.precio) != Double.doubleToLongBits(other.precio)) {
            return false;
        }
        if (this.stock != other.stock) {
            return false;
        }
        if (!Objects.equals(this.marca, other.marca)) {
            return false;
        }
        if (!Objects.equals(this.modelo, other.modelo)) {
            return false;
        }       
        if (!Objects.equals(this.añoFabricacion, other.añoFabricacion)) {
            return false;
        }
        if (!Objects.equals(this.estado, other.estado)) {
            return false;
        }
         if (!Objects.equals(this.numLlantas, other.numLlantas)) {
            return false;
        }
        return true;
    }
}
