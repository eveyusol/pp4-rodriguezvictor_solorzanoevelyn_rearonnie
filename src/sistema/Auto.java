package sistema;

import concesionaria.Color;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author eveyusol
 */
public class Auto extends Vehiculo{
    //atributos propios de la clase automovil
    private final int numAsientos;
    private final char convertible;
    private final char camRetro;
    
    public Auto(String marca, String modelo, String añoFabricacion, double precio, String estado, int numLlantas,int stock, TipoCombustible tipoCombustible,int numAsientos, char convertible, char camRetro){
        super(marca, modelo, añoFabricacion, precio, estado,numLlantas,stock, tipoCombustible);
        this.numAsientos = numAsientos;
        this.convertible = convertible;
        this.camRetro = camRetro;
    }
    //metodos getter de los atributos con el fin de acceder en otras clases

    public int getNumAsientos() {
        return numAsientos;
    }

    public char getConvertible() {
        return convertible;
    }

    public char getCamRetro() {
        return camRetro;
    }
    
    //metodo toSrting

    @Override
    public String toString() {
        return "Auto{" + "numAsientos=" + numAsientos + ", convertible=" + convertible + ", camRetro=" + camRetro + '}';
    }

    @Override
    public int hashCode() {
        int hash = 3;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Auto other = (Auto) obj;
        if (this.numAsientos != other.numAsientos) {
            return false;
        }
        if (this.convertible != other.convertible) {
            return false;
        }
        if (this.camRetro != other.camRetro) {
            return false;
        }
        return true;
    }
    
    //verificar si el vehiculo sigue en stock
    public boolean hayStock(int cantidad) {
        if((this.getStock() - cantidad)<0){
            java.lang.System.out.println();
            java.lang.System.out.println(Color.ANSI_RED + "No hay stock");
            java.lang.System.out.println("");
            java.lang.System.out.println(Color.ANSI_RED + "Solo existen "+ String.valueOf(this.getStock())+" unidades disponibles");
            return false;
        }
        java.lang.System.out.println();
        java.lang.System.out.println("Si hay stock\n");
        return true;
        
    }
    
    
    
}
