/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistema;

import concesionaria.Color;
import java.util.Objects;

/**
 *
 * @author eveyusol
 */
public class Motocicleta extends Vehiculo{
    //private final String motor;
    private String categoriaMoto;
    public Motocicleta(String marca, String modelo, String añoFabricacion, double precio, String estado, int numLlantas, int stock, TipoCombustible tipoCombustible, String categoriaMoto){
        super(marca, modelo, añoFabricacion, precio ,estado,4,stock, tipoCombustible);
        //this.motor = "gasolina";
        this.categoriaMoto = categoriaMoto;
    }

    public String getCategoria() {
        return categoriaMoto;
    }

    public void setCategoria(String categoria) {
        this.categoriaMoto = categoria;
    }

    @Override
    public String toString() {
        return "Motocicleta{" + "categoria=" + categoriaMoto + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Motocicleta other = (Motocicleta) obj;
        if (!Objects.equals(this.categoriaMoto, other.categoriaMoto)) {
            return false;
        }
        return true;
    }
    
    public boolean hayStock(int cantidad) {
        if((this.getStock() - cantidad)<0){
            java.lang.System.out.println();
            java.lang.System.out.println(Color.ANSI_RED + "No hay stock");
            java.lang.System.out.println("");
            java.lang.System.out.println(Color.ANSI_RED + "Solo existen "+ String.valueOf(this.getStock())+" unidades disponibles");
            return false;
        }
        java.lang.System.out.println();
        java.lang.System.out.println("Si hay stock\n");
        return true;
        
    }
    
    
}
