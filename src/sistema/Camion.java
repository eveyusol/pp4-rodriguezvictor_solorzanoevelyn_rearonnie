/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistema;

import concesionaria.Color;

/**
 *
 * @author eveyusol
 */
public class Camion extends Vehiculo {
    //private final String motor;
    //capacidad en kilogramos
    private final double capacidad;
    //private final int ejes;
    public Camion (String marca, String modelo, String añoFabricacion, double precio,String estado, int numLlantas,int stock, TipoCombustible tipoCombustible, double capacidad){
        super(marca, modelo, añoFabricacion, precio, estado,numLlantas,stock, tipoCombustible);
        //this.motor = motor;
        this.capacidad = capacidad;
        //this.ejes = numLlantas/2; 
    }
    
    //calculando el numero de ejes de un camion
    public int calcularEjeCamion(){
        int eje=getNumLlantas()/2;
        return eje;
    }
    //metodo get del uncio atributo de camion

    public double getCapacidad() {
        return capacidad;
    }

    @Override
    public String toString() {
        return "Camion{" + "capacidad=" + capacidad + '}';
    }

    @Override
    public int hashCode() {
        int hash = 3;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Camion other = (Camion) obj;
        if (Double.doubleToLongBits(this.capacidad) != Double.doubleToLongBits(other.capacidad)) {
            return false;
        }
        return true;
    }
    
    public boolean hayStock(int cantidad) {
        if((this.getStock() - cantidad)<0){
            java.lang.System.out.println();
            java.lang.System.out.println(Color.ANSI_RED + "No hay stock");
            java.lang.System.out.println("");
            java.lang.System.out.println(Color.ANSI_RED + "Solo existen "+ String.valueOf(this.getStock())+" unidades disponibles");
            return false;
        }
        java.lang.System.out.println();
        java.lang.System.out.println("Si hay stock\n");
        return true;
        
    }
    
}
