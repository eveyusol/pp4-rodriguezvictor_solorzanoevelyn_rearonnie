package sistema;


import acceso.Menu;
import concesionaria.Stock;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import usuarios.User;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import usuarios.Cliente;
import usuarios.JefeTaller;
import usuarios.Supervisor;
//import com.google.gson.Gson;
//import com.google.gson.GsonBuilder;
import usuarios.Vendedor;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author eveyusol
 */
public class Sistema {
    public static ArrayList<Vendedor> listaVendedores = new ArrayList<Vendedor>();
    public static ArrayList<Vendedor> listaVendedoresAleatorios = new ArrayList<Vendedor>();
    public static ArrayList<Cliente> listaCliente = new ArrayList<Cliente>();
    public static ArrayList<Supervisor> listaSupervisor = new ArrayList<Supervisor>();
    public static ArrayList<JefeTaller> listaJefeTaller = new ArrayList<JefeTaller>();
    
    /**
     * @param args the command line arguments
     */
    
    public static void main(String[] args) throws IOException {
        Auto a = new Auto("Chevrolet","Aveo","2010",10000,"Excelente",4,5,TipoCombustible.GASOLINA,5,'1','0');
        Camion c = new Camion("Mercedez Benz","G01","2010",70000,"Excelente",4,5,TipoCombustible.DIESEL,10000);
        Motocicleta m = new Motocicleta("Yamaha","b01X","2010",3500,"Excelente",4,5,TipoCombustible.GASOLINA,"Carrera");
        Tractores t = new Tractores("John Deere","AX123","2018",10000,"Excelente",4,5,TipoCombustible.GASOLINA,'1','1');
        //Agregando los vehiculos a los mdoelos disponibles
        Stock.modelosDisponibles.add(a);
        Stock.modelosDisponibles.add(c);
        Stock.modelosDisponibles.add(m);
        Stock.modelosDisponibles.add(t);        
        Menu.MenuInicial();
        // TODO code application logic here
       /*
        //Menu.MenuInicial();
        Cliente cliente=new Cliente("yuni","Burgos","mi crush no me quiere", "201500832");
        //GSON 
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        String jsonEjemplo = gson.toJson(cliente);
        System.out.println(jsonEjemplo);  
        serializar(cliente);
        deserializar();
        
        
        
    }
    /**
    
     //revisar serializar
    public static void  serializar(Cliente c) throws IOException{
        FileOutputStream f= null;
                    ObjectOutputStream s=null;
        try {
             f= new FileOutputStream("Clientes.json");
            s = new ObjectOutputStream(f);
            s.writeObject(c);
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Sistema.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Sistema.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally{
            f.close();
            s.close();
        }
    }
    public static Cliente deserializar(){
        try(ObjectInputStream o = new ObjectInputStream( new FileInputStream("Clientes.json"))){
            Cliente c= (Cliente)o.readObject();
            java.lang.System.out.println("Deserializado"+c);
            return c;
        }
       catch (FileNotFoundException ex) {
            Logger.getLogger(Sistema.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Sistema.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Sistema.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    */
}
}
