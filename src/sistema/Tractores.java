/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistema;

import concesionaria.Color;


/**
 *
 * @author eveyusol
 */
public class Tractores extends Vehiculo{
    private char transmision; // si vale 1 es hidráulica, si es 0 es mecánica
    private char uso; //1 es Agricola, 0 no es agricola
    //private String motor;
    
    public Tractores(String marca, String modelo, String añoFabricacion, double precio, String estado, int numLlantas, int stock, TipoCombustible tipoCombustible, char transmision, char uso){
        super(marca, modelo, añoFabricacion, precio,estado,4,stock, TipoCombustible.DIESEL);
        this.transmision = transmision;
        this.uso = uso;
        //this.motor = motor;   
    }

    public char getTransmision() {
        return transmision;
    }

    public void setTransmision(char transmision) {
        this.transmision = transmision;
    }

    public char getUso() {
        return uso;
    }

    public void setUso(char uso) {
        this.uso = uso;
    }

    @Override
    public String toString() {
        return "Tractores{" + "transmision=" + transmision + ", uso=" + uso + '}';
    }

    @Override
    public int hashCode() {
        int hash = 5;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Tractores other = (Tractores) obj;
        if (this.transmision != other.transmision) {
            return false;
        }
        if (this.uso != other.uso) {
            return false;
        }
        return true;
    }
    public boolean hayStock(int cantidad) {
        if((this.getStock() - cantidad)<0){
            java.lang.System.out.println();
            java.lang.System.out.println(Color.ANSI_RED + "No hay stock");
            java.lang.System.out.println("");
            java.lang.System.out.println(Color.ANSI_RED + "Solo existen "+ String.valueOf(this.getStock())+" unidades disponibles");
            return false;
        }
        java.lang.System.out.println();
        java.lang.System.out.println("Si hay stock\n");
        return true;
        
    }
}
